package fr.cnam.foad.nfa035.badges.service.impl;


import fr.cnam.foad.nfa035.badges.service.BadgesWalletRestService;
import fr.cnam.foad.nfa035.badges.wallet.dao.impl.json.JSONBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Set;
import java.util.SortedMap;

@RestController
public class BadgesWalletRestServiceImpl implements BadgesWalletRestService{

    @Qualifier("jsonBadge")
    @Autowired
    JSONBadgeWalletDAO jsonBadgeDao;


    /**
     * {@inheritDoc}
     * @param badge
     * @param file
     * @return
     */
    @Override
    public ResponseEntity putBadge(@RequestPart DigitalBadge badge, @RequestPart MultipartFile file) {
        try {
            SortedMap<DigitalBadge, DigitalBadgeMetadata> badges = jsonBadgeDao.getWalletMetadataMap();
            if (badges.containsKey(badge)) {
            }

            return ResponseEntity.ok().build();
        } catch (IOException e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().build();
        }
    }

    /**
     * {@inheritDoc}
     * @return
     * @throws IOException
     */
    @Override
    public ResponseEntity<Set<DigitalBadge>> getMetadata() throws IOException {
        try {
            return ResponseEntity.ok().body(jsonBadgeDao.getWalletMetadata());

        } catch (IOException e) {
            e.printStackTrace();
            return ResponseEntity.notFound().build();
        }
    }

    /**
     * {@inheritDoc}
     * @param badge
     * @return
     */
    @Override
    public ResponseEntity deleteBadge(@RequestPart DigitalBadge badge) {
        try{
            SortedMap<DigitalBadge,DigitalBadgeMetadata> badges = jsonBadgeDao.getWalletMetadataMap();
            if (badges.containsKey(badge)){

            }
            else {
                return ResponseEntity.notFound().build();
            }
            return ResponseEntity.ok().build();
        } catch (IOException e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().build();
        }


    }
}



