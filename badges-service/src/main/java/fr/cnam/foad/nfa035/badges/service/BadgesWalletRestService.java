package fr.cnam.foad.nfa035.badges.service;


import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Set;

@RequestMapping("/_badges")
public interface BadgesWalletRestService {

    /**
     *  Ajouter ou mise-à-jour d'un Badge
     * @param badge
     * @param file
     * @return
     */

    @Operation(summary = "ajoute le badge au wallet",
            description = "ajoute un nouveau badge au portefeuille, s'il existe déjà, le met à jour"
    )
    @Tag(name = "putBadge")
    @PutMapping
    ResponseEntity putBadge(DigitalBadge badge, MultipartFile file);

    /**
     * Lecture du Wallet => R
     *
     * @return ResponseEntity la réponse REST toujours
     */
    @Operation(summary = "Récupère le métadonnées du Wallet",
            description = "Récupère le métadonnées du portefeuille, c'est à dire l'index des badges qui s'y trouve"
    )

    @Tag(name = "getMetadata")
    @GetMapping("/metas")
    ResponseEntity<Set<DigitalBadge>> getMetadata() throws IOException;

/**
 * Supprimer d'un badge
 */
    @Operation(summary = "Supprime un Badge du Wallet",
            description = "Supprime le badge, s'il est présent, du portefeuille"
    )
    @Tag(name = "deleteBadge")
    @DeleteMapping
    ResponseEntity deleteBadge(DigitalBadge badge);





}
